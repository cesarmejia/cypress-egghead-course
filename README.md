# 1. Cypress Egghead Course

<!-- TOC -->

- [Cypress Egghead Course](#cypress-egghead-course)
  - [Checklist](#checklist)
  - [Notes](#notes)

<!-- /TOC -->

## 1.1. Checklist

- [x] ~~_Lesson 01: Introduction_~~ [2020-01-20]
- [x] ~~_Lesson 02: Install Cypress_~~ [2020-01-20]
- [x] ~~_Lesson 03: Setup Cypress Dev Env_~~ [2020-01-20]
- [x] ~~_Lesson 04: Write Your First Cypress Integration Test_~~ [2020-01-20]
- [x] ~~_Lesson 05: The Most Robust Selector_~~ [2020-01-20]
- [x] ~~_Lesson 06: Debug and Log_~~ [2020-01-20]
- [x] ~~_Lesson 07: Mock your Backend_~~ [2020-01-20]
- [x] ~~_Lesson 08: Assert Redux store_~~ [2020-01-20]
- [x] ~~_Lesson 09: Create Custom Commands_~~ [2020-01-20]
- [x] ~~_Lesson 10: Wrap External Libraries_~~ [2020-01-20]
- [x] ~~_Lesson 11: Fixtures_~~ [2020-01-20]
- [x] ~~_Lesson 12: Mock Network Retries_~~ [2020-01-20]
- [x] ~~_Lesson 13: Unstubbed Cypress Request with Force 404_~~ [2020-01-20]
- [x] ~~_Lesson 14: Cypress Plugins_~~ [2020-01-20]
- [x] ~~_Lesson 15: Seed your DB_~~ [2020-01-20]
- [x] ~~_Lesson 16: Productionize Seeder_~~ [2020-01-20]
- [x] ~~_Lesson 17: Assert on DB Snapshots_~~ [2020-01-20]
- [x] ~~_Lesson 18: Assert on XHR Requests_~~ [2020-01-20]
- [x] ~~_Lesson 19: E2E_~~ [2020-01-20]

## 1.2. Notes

- `data-cy='description-of-target'`: Use data-cy attributes to target DOM elements
- `.debug()` to run the debugger
- `cy.window()` and `window.Cypress` are useful to gain access to global objects (e.g. Redux store)
- **getStateSlice** custom command:

```javascript
Cypress.Commands.add('store', (stateName = '') => {
  const log = Cypress.log({ name: 'store' })

  const cb = state => {
    log.set({ message: JSON.stringify(state), consoleProps: () => state })
    return state
  }

  cy.window({ log: false })
    .then($window => $window.store.getState())
    .then(state => {
      if (stateName.length > 0) {
        return cy
          .wrap(state, { log: false })
          .its(stateName)
          .then(cb)
      } else {
        return cy.wrap(state, { log: false }).then(cb)
      }
    })
})
```

- Handy `lo_filter`:

```javascript
Cypress.Commands.add('lo_filter', { prevSubject: true }, (subject, fn) => {
  const result = subject.filter(fn)
  Cypress.log({ name: 'lo_filter', message: JSON.stringify(result), consoleProps: () => result })
  return result
})
```

- `cy.server({force404: true})` to generated 404 error on any non-stubbed xhr request
- Asserting against XHR Request:

```javascript
cy.wait('@load').then(xhr => {
  cy.wrap(xhr.status).should('be.equal', 200)
  cy.wrap(xhr.method).should('be.equal', 'GET')
  cy.wrap(xhr.response.body).should('deep.equal', [
    {
      text: 'test',
      completed: false,
      id: 1,
    },
    {
      text: 'test2',
      completed: true,
      id: 2,
    },
  ])
})
```
