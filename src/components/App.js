import React, { useEffect } from 'react'
import Header from '../containers/Header'
import MainSection from '../containers/MainSection'

const App = ({ store }) => {
  useEffect(() => {
    store.dispatch({ type: 'FETCH_TODOS' })
  }, [])

  return (
    <div>
      <Header />
      <MainSection />
    </div>
  )
}

export default App
