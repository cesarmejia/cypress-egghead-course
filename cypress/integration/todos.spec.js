describe('Todo Application', () => {
  it('loads the page', () => {
    cy.server()
    cy.route('GET', '/api/todos', [
      {
        text: 'test',
        completed: false,
        id: 1,
      },
      {
        text: 'test2',
        completed: true,
        id: 2,
      },
    ]).as('load')

    cy.visit('/')
    cy.wait('@load').then(xhr => {
      cy.wrap(xhr.status).should('be.equal', 200)
      cy.wrap(xhr.method).should('be.equal', 'GET')
      cy.wrap(xhr.response.body).should('deep.equal', [
        {
          text: 'test',
          completed: false,
          id: 1,
        },
        {
          text: 'test2',
          completed: true,
          id: 2,
        },
      ])
    })

    cy.store('todos')
      .lo_filter(todo => todo.id === 1)
      .should('deep.equal', [
        {
          text: 'test',
          completed: false,
          id: 1,
        },
      ])

    cy.get('[data-cy=todo-item-1]')
      .should('have.text', 'test')
      .should('not.have.class', 'completed')
      .find('.toggle')
      .should('not.be.checked')

    cy.get('[data-cy=todo-item-2]')
      .should('have.text', 'test2')
      .should('have.class', 'completed')
      .find('.toggle')
      .should('be.checked')
  })
})
