// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add('store', (stateName = '') => {
  const log = Cypress.log({ name: 'store' })

  const cb = state => {
    log.set({ message: JSON.stringify(state), consoleProps: () => state })
    return state
  }

  cy.window({ log: false })
    .then($window => $window.store.getState())
    .then(state => {
      if (stateName.length > 0) {
        return cy
          .wrap(state, { log: false })
          .its(stateName)
          .then(cb)
      } else {
        return cy.wrap(state, { log: false }).then(cb)
      }
    })
})

Cypress.Commands.add('lo_filter', { prevSubject: true }, (subject, fn) => {
  const result = subject.filter(fn)
  Cypress.log({ name: 'lo_filter', message: JSON.stringify(result), consoleProps: () => result })
  return result
})
